
function staringValidator(userInput) {
    if (!validCommand(userInput)) {
        return 500;
    }else if (!isCommand(userInput)) {
        return 300;
    }else if (multipleCommands(userInput)) {
        return 400;
    }
    return 200;
}

function isCommand(userInput) {
    if (userInput.charAt(0) === '/') {
        let char = userInput.charAt(1);
        if (char.toLowerCase() !== char.toUpperCase()) {    // If false, the character following "/" is a letter
            return true;
        }
    }
    return false;
}

function validCommand(userInput) {
    return userInput.includes('/') && !userInput.includes("//");
}

function multipleCommands(userInput) {
    return userInput.substr(1).includes('/');
}

/*
    Codes Meaning:
    200 - OK
    300 - does not start with command line ("/") (Not a command)
    400 - multiple commands
    500 - Multiple "//" in a row (invalid command)
*/

module.exports = {
    staringValidator
}
