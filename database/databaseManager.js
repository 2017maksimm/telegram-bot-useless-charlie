const mongoose = require("mongoose");
const Agenda = require("agenda");
const MongoCron = require('mongodb-cron').MongoCron;

require('dotenv').config();
let agenda;

async function connectToDB() {
    mongoose.connect(
        `mongodb+srv://root:${process.env.MONGODB_PASSWORD}@cluster0.dbi9p.mongodb.net/${process.env.DATABASE_NAME}?retryWrites=true&w=majority`,
        {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }
    ).then().catch(err => console.log("Error Occurred: " + err));

    //  Check Successful Connection
    const db = mongoose.connection;
    db.on("error", console.error.bind(console, "connection error: "));
    db.once("open", function () {
        console.log("Connected successfully");
    });
    //
    // const collection = db.collection('reminders');
    // const cron = new MongoCron({
    //     collection, // a collection where jobs are stored
    //     onDocument: async (doc) => console.log(doc), // triggered on job processing
    //     onError: async (err) => console.log(err), // triggered on error
    // });
    //
    // await cron.start();
    //
    // const job = await collection.insert({
    //     sleepUntil: new Date(Date.now() + 1000), // ISO 8601 format (can include timezone)
    //
    // });

    // agenda = new Agenda().mongo(db, "TELEGRAM_BOT_DB");
    //
    // agenda.define('execute_reminder', async job => {
    //     const { test } = job.attrs;
    //     console.log("Testing Tests " + test);
    // });
    //
    // await agenda.schedule(new Date(Date.now() + 1000), 'execute_reminder', test = 'This is dumb!');
    // await new Promise(resolve => agenda.once('ready', resolve));
    //await agenda.start();


}

module.exports = {
    connectToDB
}
