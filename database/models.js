const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    _id: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true,
    },
    conversationState: {
        //  0 - default state

        //  Default reminder states
        //  101 - waiting for user to submit event
        //  102 - waiting for user to submit event date
        type: Number,
        default: 0,
    },
    userTimezone: {
        type: String,
        required: false
    }
});

const User = mongoose.model("User", userSchema);

module.exports = {
    User
}
