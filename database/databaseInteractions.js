const models = require("./models.js");
const Users = models.User;

async function userExists(id){
    return await Users.exists({_id: id}).catch(err => console.log(err));
}

async function addUser(id, name){
    let newUser = new Users({
        _id: id,
        name: name
    });
    await newUser.save().catch(err => console.log(err));
}

async function changeUserState(userId, newState) {
    await Users.findByIdAndUpdate({_id: userId}, {conversationState: newState});
}

async function getUserState(userId) {
    let user = await Users.findById(userId);
    return user.conversationState;
}

async function setUserTimezone(userId, timezone) {
    await Users.findByIdAndUpdate({_id: userId}, {userTimezone: timezone});
}

let reminders = {}

module.exports = {
    userExists,
    addUser,
    changeUserState,
    getUserState,
    setUserTimezone,
    reminders
}


