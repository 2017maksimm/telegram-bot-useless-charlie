const TelegramBot = require("node-telegram-bot-api");
const moviesSection = require("./movies/replyFormat.js");
const inputValidator = require("./errorMessages/userInputValidator.js");
const database = require("./database/databaseInteractions.js");
const schedule = require("node-schedule");

require("./database/databaseManager.js").connectToDB().catch(err => console.log(err));
require("dotenv").config();

const token = process.env.TELEGRAM_TOKEN;
let bot = new TelegramBot(token, {polling: true});

bot.on("message", async function(msg) {

    console.log(msg);

    if (!await database.userExists(msg.chat.id) || await database.getUserState(msg.chat.id) === 0) {
        let inputCode = inputValidator.staringValidator(msg.text);
        if (inputCode !== 200) {
            await bot.sendMessage(msg.chat.id, codeToReply(inputCode));
            return;
        }
        switch (true) {
            case msg.text.includes("/movie"):
                if (msg.text.split(' ').length !== 2) {
                    await bot.sendMessage(msg.chat.id, `I'm sorry, I'm not sure what you mean... If you want me to find you a movie, provide following command "/movie (movie genre)". For example: "/movie action". To see all genres that I know, write "/genres"`);
                }else {
                    let response = await moviesSection.getMovieByGenre(msg.text.split(' ')[1]);
                    await bot.sendMessage(msg.chat.id, response);
                }
                break;

            case msg.text.includes("/reminder"):
                await database.changeUserState(msg.chat.id, 101);
                await bot.sendMessage(msg.chat.id, "What do you want me to remind you of?");
                break;

            case msg.text.includes("/genres"):
                let userResponse = await moviesSection.getGenres();
                await bot.sendMessage(msg.chat.id, userResponse);
                break;

            case msg.text.includes("/start"):
                if (!await database.userExists(msg.chat.id)) {
                    await database.addUser(msg.chat.id, msg.chat.first_name);
                    console.log("User Created");
                }else {
                    console.log("User Exists");
                }
                await bot.sendMessage(msg.chat.id, "Hello! My name is Charlie! I am a baby-bot made by an idiot, so please don't be mad " +
                    "at me if I happen to make a boo boo :( Here is a list of commands I understand:\n" +
                    "/movie (genre) - Randomly chooses a movie with a good rating from TMDB\n/genres - lists genres that I understand\n" +
                    "/help - lists commands that I understand");
                break;

            case msg.text.includes("/help"):
                await bot.sendMessage(msg.chat.id, 'Here is a list of commands I understand: /movie (genre) - Randomly chooses a movie with a good rating from TMDB /genres - lists genres that I understand /help - well... You can see what it does...');
                break;

            case msg.text.includes("/setTimezone"):
                if (msg.text.split(' ').length !== 2) {
                    await bot.sendMessage(msg.chat.id, `I'm sorry, I'm not sure what you mean... If you want me to find set a timezone for you, write: "/setTimezone (timezone)". Example: "/setTimezone GMT+2"`);
                }else {
                    //  Validate Timezone!!!
                    await database.setUserTimezone(msg.chat.id, msg.text.split(' ')[1]);
                    await bot.sendMessage(msg.chat.id, "Timezone Set!");
                }
                break;
        }
    }else {
        switch (await database.getUserState(msg.chat.id)) {
            case 101:
                database.reminders[msg.chat.id] = msg.text;
                await database.changeUserState(msg.chat.id, 102);
                await bot.sendMessage(msg.chat.id, `When should I remind you? Enter date/time in the following format: "DD Month YYYY hh:mm(optional) TIMEZONE(optional)" eg: "03 Jan 2022 15:30 GMT+2"`);
                break;
            case 102:
                if (isNaN(Date.parse(msg.text))) {
                    await bot.sendMessage(msg.chat.id, "Sorry, the date/time you provided is invalid :(\nTry again");
                }else {
                    schedule.scheduleJob(new Date(Date.parse(msg.text)), async function () {
                        let reminder = database.reminders[msg.chat.id];
                        await bot.sendMessage(msg.chat.id, `Hi, you asked me to remind you of the following: "${reminder}"`);
                        delete database.reminders[msg.chat.id];
                    });
                    await database.changeUserState(msg.chat.id, 0);
                }
                break;
        }
    }
});

function codeToReply(errorCode) {
    switch (errorCode) {
        case 300:
            return "Sorry, this input is not a command. For a list of recognisable commands type \"/help\"";
        case 400:
            return "Sorry, multiple commands have been detected. For a list of recognisable commands type \"/help\"";
        case 500:
            return "Sorry, this command is invalid. For a list of recognisable commands type \"/help\"";
        default:
            return "Sorry, unrecognised error occurred";
    }
}
