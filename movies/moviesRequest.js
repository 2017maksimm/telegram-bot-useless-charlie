const fetch = require('node-fetch');

function getMoviesByGenre(genreId) {
    return new Promise(resolve => {
        fetch(`https://api.themoviedb.org/3/discover/movie?sort_by=vote_average.desc&api_key=${process.env.TMDB_TOKEN}&with_genres=${genreId}&vote_count.gte=100`)
            .then(res => res.json())
            .then(json => resolve (json));
    })
}

function getMovieGenres() {
    return new Promise(resolve => {
        fetch(`https://api.themoviedb.org/3/genre/movie/list?api_key=${process.env.TMDB_TOKEN}&language=en-US`)
            .then(res => res.json())
            .then(json => resolve (json));
    })
}

module.exports = {
    getMoviesByGenre,
    getMovieGenres
}
