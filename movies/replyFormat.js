const moviesRequest = require('./moviesRequest.js');

async function getGenres() {
    let json = await moviesRequest.getMovieGenres();
    let reply = "";
    for (let i = 0; i < json.genres.length; i++) {
        if (json.genres[i].id === 878) {
            reply += "Sci-Fi\n";
        }else if (json.genres[i].id !== 10770) {
            reply += json.genres[i].name + "\n";
        }
    }
    return reply;
}

async function getMovieByGenre(input) {
    if (genreToNumber(input.toLowerCase()) === 1) {
        return "Sorry, I don't understand this genre. For a list of valid genres type\"/genres\"";
    }else {
        let json = await moviesRequest.getMoviesByGenre(genreToNumber(input.toLowerCase()));
        let randomMovie = json.results[getRandomInt(json.results.length)];
        return randomMovie.title + "\n" + randomMovie.overview + "\n" + randomMovie.vote_average;
    }
}

function genreToNumber(genre) {
    switch (genre) {
        case "action":
            return 28;
        case "adventure":
            return 12;
        case "animation":
            return 16;
        case "comedy":
            return 35;
        case "crime":
            return 80;
        case "documentary":
            return 99;
        case "drama":
            return 18;
        case "family":
            return 10751;
        case "fantasy":
            return 14;
        case "history":
            return 36;
        case "horror":
            return 27;
        case "music":
            return 10402;
        case "mystery":
            return 9648;
        case "romance":
            return 10749;
        case "sci-fi":
            return 878;
        case "thriller":
            return 53;
        case "war":
            return 10752;
        case "western":
            return 37;
        default:
            return 1;
    }
}

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

module.exports = {
    getGenres,
    getMovieByGenre
}
